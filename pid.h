#ifndef _PID_H_
#define _PID_H_

class PID
{
    public:
        // Kp -  proportional gain
        // Ki -  Integral gain
        // Kd -  derivative gain
        // dt -  loop interval time
        // max - maximum value of manipulated variable
        // min - minimum value of manipulated variable
        PID(double Kp, double Ki, double Kd, double dt);
        PID(double Kp, double Ki, double Kd, double dt, double max, double min);

        // Returns the manipulated variable given a setpoint and current process value
        double calculate(double sp, double pv);
        ~PID();

    private:
        double _Kp;
        double _Ki;
        double _Kd;
        double _dt;
        double _max;
        double _min;
        double _pre_error;
        double _integral;
        bool _limited;
};

#endif
