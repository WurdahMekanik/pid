#include <iostream>
#include <cmath>
#include "pid.h"

using namespace std;

PID::PID(double Kp, double Ki, double Kd, double dt) :
    _Kp(Kp),
    _Ki(Ki),
    _Kd(Kd),
    _dt(dt),
    _pre_error(0),
    _integral(0),
    _limited(false)
{
}

PID::PID(double Kp, double Ki, double Kd, double dt, double max, double min) :
    _Kp(Kp),
    _Ki(Ki),
    _Kd(Kd),
    _dt(dt),
    _max(max),
    _min(min),
    _pre_error(0),
    _integral(0),
    _limited(true)
{
}

double PID::calculate(double sp, double pv)
{
    
    // Calculate error
    double error = sp - pv;

    // Proportional term
    double Pout = _Kp * error;

    // Integral term
    _integral += error * _dt;
    double Iout = _Ki * _integral;

    // Derivative term
    double derivative = (error - _pre_error) / _dt;
    double Dout = _Kd * derivative;

    // Calculate total output
    double output = Pout + Iout + Dout;

    // Restrict to max/min
    if (_limited) {
        if (output > _max) {
            output = _max;
        } else if (output < _min) {
            output = _min;
        }
    }

    // Save error term to previous error
    _pre_error = error;

    return output;
}

PID::~PID()
{
}

